/* eslint-disable import/extensions */
// eslint-disable-next-line no-multiple-empty-lines

import { expect } from 'chai';

import {
  describe, beforeEach, afterEach, it,
} from 'mocha';

import NumbersValidator from '../../app/numbers_validator.js';

describe('isNumberEven', () => {
  let validator;

  beforeEach(() => {
    validator = new NumbersValidator();
  });

  afterEach(() => {
    validator = null;
  });

  it('should return true if number is even', () => {
    expect(validator.isNumberEven(4)).to.be.equal(true);
  });

  it('should return false if number is odd', () => {
    expect(validator.isNumberEven(5)).to.be.equal(false);
  });

  it('should throw an error in case of a string', () => {
    expect(() => {
      validator.isNumberEven('4');
    }).to.throw('[4] is not of type "Number" it is of type "string"');
  });
});

describe('getEvenNumbersFromArray', () => {
  let validator;

  beforeEach(() => {
    validator = new NumbersValidator();
  });

  afterEach(() => {
    validator = null;
  });

  it('should return an array of even numbers from the given array', () => {
    expect(validator.getEvenNumbersFromArray([1, 2, 3, 4, 5])).to.deep.equal([2, 4]);
  });

  it('should return an empty array if no even numbers are found', () => {
    expect(validator.getEvenNumbersFromArray([1, 5, 7])).to.deep.equal([]);
  });

  it('should throw an error if the input is not an array of numbers', () => {
    expect(() => {
      validator.getEvenNumbersFromArray('1, 2, 3');
    }).to.throw('[1, 2, 3] is not an array of "Numbers"');
  });
});
describe('isAllNumbers', () => {
  let validator;

  beforeEach(() => {
    validator = new NumbersValidator();
  });

  afterEach(() => {
    validator = null;
  });

  it('should return true if all elements in the array are numbers', () => {
    expect(validator.isAllNumbers([1, 2, 3, 4, 5])).to.be.equal(true);
  });

  it('should return false if the array contains non-number elements', () => {
    expect(validator.isAllNumbers([1, '2', 3])).to.be.equal(false);
  });

  it('should throw an error if the input is not an array', () => {
    expect(() => {
      validator.isAllNumbers('1, 2, 3');
    }).to.throw('[1, 2, 3] is not an array');
  });
});

describe('isInteger', () => {
  let validator;

  beforeEach(() => {
    validator = new NumbersValidator();
  });

  afterEach(() => {
    validator = null;
  });

  it('should return true if the number is an integer', () => {
    expect(validator.isInteger(10)).to.be.equal(true);
  });

  it('should return false if the number is not an integer', () => {
    expect(validator.isInteger(8.5)).to.be.equal(false);
  });

  it('should throw an error if the input is not a number', () => {
    expect(() => {
      validator.isInteger('3');
    }).to.throw('[3] is not a number');
  });
});
